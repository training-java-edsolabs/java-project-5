package com.company.client;


import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

import static com.company.server.WorkerThread.FILE_TO_SEND_1;

public class EchoChatClient {
    public final static String SERVER_IP = "127.0.0.1";
    public final static int SERVER_PORT = 7;
    public final static String FILE_TO_RECEIVED = "src/com/company/file/file-download/image1_download.jpg";
    public static Path path = Paths.get(FILE_TO_SEND_1);

    public static void main(String[] args) throws IOException, InterruptedException {
        Socket clientSocket = null;
        int current = 0;
        FileOutputStream fos = null;
        BufferedOutputStream bos = null;
        int choose;
        String filePath;

        try {
            Scanner scanner = new Scanner(System.in);
            clientSocket = new Socket(SERVER_IP, SERVER_PORT);
            OutputStream os = clientSocket.getOutputStream();
            System.out.println("Connected: " + clientSocket);
            System.out.print("Choose option send(0) or receive(1): ");
            choose = Integer.parseInt(scanner.nextLine());
            System.out.println(choose);

            while (choose != 1 && choose != 0) {
                System.out.println("Choose again!");
                System.out.print("Choose option send(0) or receive(1): ");
                choose = Integer.parseInt(scanner.nextLine());
            }

            if (choose == 1) {
                // receive file

                System.out.print("Enter file path:");
                filePath = scanner.nextLine();
                PrintWriter writer = new PrintWriter(os);
                writer.println("receive");
                writer.flush();
                writer.println(filePath);
                writer.close();


//                int bytes = (int) Files.size(path);
//                InputStream is = clientSocket.getInputStream();
//                fos = new FileOutputStream(FILE_TO_RECEIVED);
//                bos = new BufferedOutputStream(fos);
//                int read;
//                while ((read = is.read()) != -1) {
//                    bos.write(read);
//                    bos.flush();
//                }
//
//                System.out.println("File " + FILE_TO_RECEIVED
//                        + " downloaded (" + current + " bytes read)");

            } else {
                System.out.println("hihi");
            }


        } catch (IOException ie) {
            System.out.println(ie);
        } finally {
            if (clientSocket != null) {
                clientSocket.close();
            }
        }
    }
}