package com.company.server;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Objects;

public class WorkerThread extends Thread {
    private Socket socket;
    FileInputStream fis = null;
    BufferedInputStream bis = null;
    OutputStream os = null;

    public final static String FILE_TO_SEND_1 =  "src/com/company/file/image1.jpg";
    public final static String FILE_TO_SEND_2 =  "src/com/company/file/image2.png";
    public final static String FILE_TO_SEND_3 =  "src/com/company/file/image3.html";

    public WorkerThread(Socket socket) {
        this.socket = socket;
    }

    public void run() {
        System.out.println("Processing: " + socket);
        ArrayList<String> request= new ArrayList<>();
        try {
            InputStream is = socket.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));
            String line = bufferedReader.readLine();
            while (line != null) {
                System.out.println(line);
                request.add(line);
                line = bufferedReader.readLine();
            }
            is.close();

            if(Objects.equals(request.get(0), "receive")){
                File myFile = new File (FILE_TO_SEND_1);
                byte [] myByteArray  = new byte [(int)myFile.length()];
                fis = new FileInputStream(myFile);
                bis = new BufferedInputStream(fis);
                bis.read(myByteArray,0,myByteArray.length);
                os = new DataOutputStream(socket.getOutputStream());
                System.out.println("Sending " + FILE_TO_SEND_1 + "(" + myByteArray.length + " bytes)");


                os.write(myByteArray);
                //os.write(myByteArray,0,myByteArray.length);
                os.flush();
                os.close();
                System.out.println("Done.");
            }

//            if(responseString.toString().equals("receive")){
//                File myFile = new File (FILE_TO_SEND_1);
//                byte [] myByteArray  = new byte [(int)myFile.length()];
//                fis = new FileInputStream(myFile);
//                bis = new BufferedInputStream(fis);
//                bis.read(myByteArray,0,myByteArray.length);
//                os = new DataOutputStream(socket.getOutputStream());
//                System.out.println("Sending " + FILE_TO_SEND_1 + "(" + myByteArray.length + " bytes)");
//
//
//                os.write(myByteArray);
//                //os.write(myByteArray,0,myByteArray.length);
//                os.flush();
//                os.close();
//                System.out.println("Done.");
//            }


        } catch (IOException e) {
            System.err.println("Request Processing Error: " + e);
        }
        System.out.println("Complete processing: " + socket);
    }
}
